import json
import os
from http import HTTPStatus
from uuid import uuid4

import boto3
from aws_lambda_powertools.event_handler.api_gateway import (
    APIGatewayRestResolver, CORSConfig, Response, content_types)
from aws_lambda_powertools.logging import Logger, correlation_paths
from aws_lambda_powertools.tracing import Tracer
from boto3.dynamodb.conditions import Key

logger = Logger()
tracer = Tracer()


def __get_table_client():
    table_name = os.getenv("DDB_TABLE_NAME")
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(table_name)
    return table


table = __get_table_client()

cors_config = CORSConfig(allow_origin="*")
app = APIGatewayRestResolver(cors=cors_config)


@app.exception_handler(ValueError)
def handle_value_error(ex: ValueError):
    metadata = {"path": app.current_event.path}
    logger.error(f"Malformed request: {ex}", extra=metadata)

    return Response(
        status_code=400,
        content_type=content_types.TEXT_PLAIN,
        body="Invalid request",
    )


@app.post("/activity/<user_id>")
def create_activity(user_id):
    data = app.current_event.json_body
    activity_item = data.get("activities")

    item = {"PK": f"USER#{user_id}", "SK": f"ACTIVITY#{uuid4()}"}

    for key in activity_item.keys():
        item[key] = activity_item[key]

    try:
        table.put_item(Item=item)
    except Exception:
        raise ValueError("Request body is not correctly formated")

    return Response(
        status_code=HTTPStatus.CREATED.value,
        content_type="application/json",
        body=HTTPStatus.CREATED.phrase,
    )


@app.delete("/activity/<user_id>/<activity_id>")
def update_activity(user_id, activity_id):
    # delete item
    try:
        table.delete_item(
            Key={"PK": f"USER#{user_id}", "SK": f"ACTIVITY#{activity_id}"}
        )
    except Exception:
        raise ValueError("Something went wrong")

    # return some noice words
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=HTTPStatus.OK.phrase,
    )


@app.get("/activity/<user_id>")
def get_activity_list(user_id):
    # query all record bellongs to that user
    response = table.query(
        KeyConditionExpression=Key("PK").eq(f"USER#{user_id}")
        & Key("SK").begins_with("ACTIVITY")
    )
    activities = []
    # arrenge results as like items.json file
    for item in response["Items"]:
        item["id"] = item["SK"].split("#")[1]

        del item["PK"]
        del item["SK"]

        activities.append(item)

    # return all the data in a dict
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=json.dumps(activities),
    )


@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
def lambda_handler(event, context):
    # if(event["identity"] is None):
    #     return Response(
    #         status_code=HTTPStatus.UNAUTHORIZED.value,
    #         body=json.dumps({
    #             "status": HTTPStatus.UNAUTHORIZED.phrase,
    #             "message": HTTPStatus.UNAUTHORIZED.description
    #         }),
    #         content_type="application/json"
    #     )

    return app.resolve(event, context)
